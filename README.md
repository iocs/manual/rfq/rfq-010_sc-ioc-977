# IOC for the X-ray detector device RFQ-010:EMR-XRS-001

## Used modules

*   [mca](https://gitlab.esss.lu.se/e3/wrappers/communication/e3-mca.git)


## Controlled devices

*   RFQ-010:EMR-XRS-001 (connected via USB)
