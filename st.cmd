require essioc
require mca

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocshLoad("$(mca_DIR)/mca_usb.iocsh", "DEVICENAME=RFQ-010:EMR-XRS-001")
